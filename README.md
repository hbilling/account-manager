# Assumtions:
  
## To compile and build
* You have JAVA8 as Java version installed. 
* Maven as build tool


## To run:
* You should have JRE8 installed.


* source code is under **\manager\src\main\java**
* junit class is under **\manager\src\test\java**
* this is plain maven project
* no database is used.
* csv file uploaded will have correct format and no data validations have been implementation. 
* Every time you run the application, fresh csv should be uploaded to initilize system with transactions.
* sample csv (**AccountTransactions.csv**) has been uploaded parallel to "manager" project folder.
* if you do not have maven to build, prebuilt executable jar "**manager.jar**" is uploaded parallel to "**manager**" project folder.




1. **How to build?**
    - From console (terminal/cmd) of your system, goto to folder manager
    - run "mvn clean install"
    - running above will build executable jar under folder >> manager/target

2. **How to run and test?**
    - Build your code following steps in 1
    
    - From console (terminal/cmd) of your system, goto to folder manager/target
    
    - run "java -jar manager.jar"
    
    - It will prompt :   
    
        ```
              Please enter CSV path to initalize system :
        ```    
      

    - Copy absolute path to the csv file with which you want to initialize the system. 
 
           E.G : C:\TestAccount\AccountTransactions.csv.
       
         ```
              Once csv is uploaded, it will print on console number of records loaded
         ```
      
      
    - Next it will prompt you to enter input option to do operation :
      
         ```
             1. Get Account Balance. 
			 2. Exit. 
         ```
      
      
    - On inputing **1** to get balance, it will prompt you to enter search values to calculate balance:
    
         ```
             Enter accountId,from date, to date :
             (E.G: ACC334455, 20/10/2018 12:00:00, 20/10/2018 19:00:00)
         ```


   
    - It will print balance in screen and number of Reversal payments included.
      
         ```	
      	    Relative balance for the period is: $xxxx
         ```
         
         ```
       	    Number of transactions included is: yyy
         ```

**Below is repo structure:**

* account-manager(git repo)
   - manager (mvn project)
      - src
      - pom.xml 	
   - AccountTransactions.csv
   - manager.jar