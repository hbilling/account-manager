package account.manager.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import account.manager.repo.AccountTransactionRepository;
import account.manager.vo.AccountTransaction;

public class AccountService {
	
	private AccountTransactionRepository accountTransactionRepository;
	
	/**
	 * Upload transaction history to initilize system
	 * @param filePath - CSV file path on local machine
	 * @return - Number of records uploaded to DB
	 */
	public Integer uploadTransactionHistory(String filePath)  throws IOException {
		
		accountTransactionRepository=new AccountTransactionRepository(); 
		
		List<AccountTransaction> list = new ArrayList<>();
		
		try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
			
		
			
			list = stream.filter(line -> !line.contains("transactionId")).map(line -> {
				
				String[] tx=line.split(",");
				
				AccountTransaction actTx= new AccountTransaction(tx[0],tx[1],tx[2],tx[3],tx[4],tx[5],"");
				
				if(tx.length>6) {
					actTx.setRelatedTransaction(tx[6]);
				}
				
				return actTx;
				
			}).collect(Collectors.toList());
 
			accountTransactionRepository.initialize(list);
	
 
		} catch (IOException ioe) {
			throw ioe;
		}
		
		System.out.format("%d transactions loaded", list.size());
		System.out.println();
		
		return list.size();
	}
	
	
	/**
	 * Fetch balance for an account between 2 dates. This method alo take into consideration Reversal type payments which are outside the selected time frame 
	 * @param accountId - Account for this balance is sorted
	 * @param from - Date from which balance is calculated. dd/MM/yyyy hh:mm:ss
	 * @param to - Date upto which balance is calculated. dd/MM/yyyy hh:mm:ss
	 * @return Account balance in negative/positive
	 */
	public Float getBalance(String accountId, String from, String to) throws Exception {
		
		 accountTransactionRepository=new AccountTransactionRepository(); 
		List<AccountTransaction> paymentTransactions =  accountTransactionRepository.findTransactionsByDates(accountId, getSearchDate(from), getSearchDate(to));
		
		AtomicReference<Float> outgoing = new AtomicReference<>();
		outgoing.set(0f);
		
		paymentTransactions.forEach(tx ->{
			outgoing.set(outgoing.get()+tx.getAmount());
		});
		
		List<AccountTransaction> revrsalTransactions =  accountTransactionRepository.findReverseTransactionsFromDate(accountId, getSearchDate(from));
		
		AtomicReference<Float> incoming = new AtomicReference<>();
		incoming.set(0f);
		
		revrsalTransactions.forEach(tx ->{
			incoming.set(incoming.get()+tx.getAmount());
		});
		
		Float balance=incoming.get().floatValue()-outgoing.get().floatValue();
		
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.format("Relative balance for the period is: $%.2f ", balance.floatValue());
		System.out.println();
		System.out.format("Number of transactions included is: %d ", revrsalTransactions.size());
		
		return balance;
	}
	
	private Date getSearchDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(date.trim(), formatter);
		
		
		return  Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
	}
	


}
