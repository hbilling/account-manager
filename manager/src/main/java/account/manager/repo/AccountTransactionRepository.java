package account.manager.repo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import account.manager.vo.AccountTransaction;
import account.manager.vo.TransactionTypes;

public class AccountTransactionRepository {

	private static List<AccountTransaction> accountTransactionDB = new ArrayList<>();

	public void initialize(List<AccountTransaction> accountTransactions) {
		accountTransactionDB.addAll(accountTransactions);
	}

	public List<AccountTransaction> findTransactionsForAccount(String accountId) {

		return accountTransactionDB.stream().filter(actTx -> actTx.getFromAccountId().equalsIgnoreCase(accountId))
				.collect(Collectors.toList());

	}

	public List<AccountTransaction> findTransactionsByDates(String accountId, Date from, Date to) {

		return accountTransactionDB.stream().filter(actTx -> actTx.getFromAccountId().equalsIgnoreCase(accountId)
				&& actTx.getCreatedAt().getTime() >= from.getTime() 
				&& actTx.getCreatedAt().getTime() <= to.getTime()
				&& actTx.getTransactionType().equalsIgnoreCase(TransactionTypes.PAYMENT.type())).collect(Collectors.toList());

	}
	
	
	public List<AccountTransaction> findReverseTransactionsFromDate(String accountId, Date from) {

		return accountTransactionDB.stream().filter(actTx -> actTx.getFromAccountId().equalsIgnoreCase(accountId)
				&& actTx.getCreatedAt().getTime() >= from.getTime()
				&& actTx.getTransactionType().equalsIgnoreCase(TransactionTypes.REVERSAL.type())).collect(Collectors.toList());

	}


}
