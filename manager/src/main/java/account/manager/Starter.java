package account.manager;

import java.util.Scanner;

import account.manager.service.AccountService;


public class Starter {
	public static void main(String[] args) {

		AccountService accountService = new AccountService();

		try (Scanner scanner = new Scanner(System.in)) {

			System.out.println("Please enter CSV path to initalize system : ");
			String csvFile = "";
			csvFile += scanner.nextLine();

			accountService.uploadTransactionHistory(csvFile);
			
			
			System.out.println("\nEnter number from below options: ");
			System.out.println();
			System.out.println("1. Get Account Balance. ");
			System.out.println("2. Exit. ");
			
			String input = "";
			input += scanner.nextLine();
			
			while(input.trim().equalsIgnoreCase("1")) {
				
				System.out.println("\nEnter accountId,from date, to date: \n (E.G: ACC334455, 20/10/2018 12:00:00, 20/10/2018 19:00:00)");
				String searchInput = "";
				
				searchInput += scanner.nextLine();
				
				String searchParams[]=searchInput.split(",");	
				
				accountService.getBalance(searchParams[0].trim(), searchParams[1].trim(), searchParams[2].trim());
				
				System.out.println();
				System.out.println("\nEnter number from below options: ");
				System.out.println();
				System.out.println("1. Get Account Balance. ");
				System.out.println("2. Exit. ");
				
				input = "";
				input += scanner.nextLine();
				
			}

	



		}catch(Exception e) {
			System.out.println("Error in processing....");
			e.printStackTrace(); 
		}
	}
}
