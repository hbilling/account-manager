package account.manager.vo;

public enum TransactionTypes {

	PAYMENT("PAYMENT"), REVERSAL("REVERSAL");

	private String type;

	TransactionTypes(String type) {
		this.type = type;
	}

	public String type() {
		return type;
	}

}
