package account.manager.vo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class AccountTransaction {
	
	private String transactionId;
	private String fromAccountId;
	private String toAccountId;
	private Date createdAt;
	private float amount;
	private String transactionType;
	private String relatedTransaction;
	
	
	public AccountTransaction() {
		super();
	}
	
	public AccountTransaction(String transactionId, String fromAccountId, String toAccountId, String createdAt,
			String amount, String transactionType, String relatedTransaction) {
		super();
		setTransactionId(transactionId);
		setFromAccountId(fromAccountId);
		setToAccountId(toAccountId);
		setCreatedAt(createdAt);
		setAmount(amount);
		setTransactionType(transactionType);
		setRelatedTransaction(relatedTransaction);
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId.trim();
	}
	public String getFromAccountId() {
		return fromAccountId;
	}
	public void setFromAccountId(String fromAccountId) {
		this.fromAccountId = fromAccountId.trim();
	}
	public String getToAccountId() {
		return toAccountId;
	}
	public void setToAccountId(String toAccountId) {
		this.toAccountId = toAccountId.trim();
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(String createdAt) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(createdAt.trim(), formatter);
		
		this.createdAt =  Date.from(dateTime.atZone(ZoneId.systemDefault()).toInstant());
	}
	public float getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = Float.valueOf(amount.trim());
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType.trim();
	}
	public String getRelatedTransaction() {
		return relatedTransaction;
	}
	public void setRelatedTransaction(String relatedTransaction) {
		this.relatedTransaction = relatedTransaction.trim();
	}
	
	
	
	

}
