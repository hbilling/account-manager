package account.manager.service;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import account.manager.repo.AccountTransactionRepository;
import account.manager.vo.AccountTransaction;
import account.manager.vo.TransactionTypes;

public class AccountServiceTest {
	
	
	public AccountService accountService;
	
	public AccountTransactionRepository accoutTransactionRepo;
	
	
	@Before
	public void initRepo() throws Exception {
		
		accountService=new AccountService();
		accoutTransactionRepo=new AccountTransactionRepository();
		
		List<AccountTransaction> accountTransactions=new ArrayList<>();
		
		accountTransactions.add(new AccountTransaction("Tx1", "1001", "1002", "12/11/2019 11:12:23", "5.5", TransactionTypes.PAYMENT.type(), ""));
		accountTransactions.add(new AccountTransaction("Tx2", "1002", "1003", "12/11/2019 13:32:11", "10", TransactionTypes.PAYMENT.type(), ""));
		accountTransactions.add(new AccountTransaction("Tx3", "1001", "1003", "12/11/2019 13:53:38", "25", TransactionTypes.PAYMENT.type(), ""));
		accountTransactions.add(new AccountTransaction("Tx4", "1003", "1002", "12/11/2019 14:12:55", "15", TransactionTypes.PAYMENT.type(), ""));
		accountTransactions.add(new AccountTransaction("Tx5", "1001", "1002", "13/11/2019 09:12:23", "5.5", TransactionTypes.REVERSAL.type(), "Tx1"));
		accountTransactions.add(new AccountTransaction("Tx6", "1001", "1002", "13/11/2019 11:43:10", "10.5", TransactionTypes.PAYMENT.type(), ""));
		accountTransactions.add(new AccountTransaction("Tx7", "1001", "1002", "14/11/2019 01:27:23", "15.5", TransactionTypes.PAYMENT.type(), ""));
		accountTransactions.add(new AccountTransaction("Tx2", "1002", "1008", "17/11/2019 13:32:11", "40", TransactionTypes.REVERSAL.type(), "tx001"));
		
		accoutTransactionRepo.initialize(accountTransactions);
	}
	
	
	@Test
	public void testGetBalance() throws Exception {
		
		Float balance=accountService.getBalance("1001", "12/11/2019 11:12:00", "13/11/2019 09:30:00");
		assertTrue(balance==-25f);
		
		balance=accountService.getBalance("1002", "12/11/2019 11:12:00", "13/11/2019 09:30:00");
		assertTrue(balance==30);
		
	}

}
